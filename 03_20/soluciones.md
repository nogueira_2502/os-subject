## Preguntas de autoevaluación:

1. Explica qué hace la secuencia de teclas Ctrl-Z

Para enviarlo a segundo plano. Y si lo haces de esta forma, se detiene el proceso.

2. Explica qué hace . jobs

Para saber todos los procesos que tienes en segundo plano.

3. Explica qué hace . bg

Para pasarlo a segundo plano.

4. Explica qué hace . fg

Para ejecutar en primer plano.

5. Graba un screencast (puedes usar obs) mostrando los comandos anteriores.



6. Explica qué hace . ps

Nos proporciona información sobre los procesos de un sistema.

7. Explica . ps aux

Para listar todos los procesos.

8. Explica . ps -ef

Para ver los procesos activos.

9. Explica qué hace y sus opciones r y a. disown

Disown: Es para ejecutar, equibalente a disown %5 . Si vuleves a ejecutar disown, quitarás un proceso.
disown -a: Comprobarás que todos los procesos desaparecen de la tabla.
disown -r: Para matar esos procesos.

10. Explica el comando . kill

Para matar el proceso.

11. Explica el comando . pkill

Para detener todos los procesos.

12. Explica el comando . nohup

Viene de la palabra "No Hangups". Este comando sigue funcionando hasta que se completa, incluso si si el duño del ordenador cierra sesión.

### Haz un screencast para documentarlo.

1. Abre una conexión a tu ordenador: ssh ssh localhost

2. En esa sesión el comando ./tiempo &

3. Sal de la sesión escribiendo .exit

4. Muestra desde la terminal con que el programa sigue corriendo.ps aux | grep tiempo

