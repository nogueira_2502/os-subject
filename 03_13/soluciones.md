#Viernes 13

## Grupos.

1. Explica cada uno de los campos de ésta línea de fichero/etc/group. sambasharex:129:lorenzo,pepe,juan

Sambashare: Nombre del grupo.
x: Una contraseña cifrada.
129: El número de indentificación del grupo GID.
lorenzo,pepe,juan: Es un listado de los ususarios que pertenecen al grupo separado por comas.

2. Lista sólo los grupos de tú equipo.

cut -d : -f 1 /etc/group

3. Añade el grupo de juego y ocio a tu equipo usando una sola instrucción.

sudo groupadd jugo, ocio

4. Borra el usuario ocio.

sudo deluser ocio

## Usuarios.

1. Explica cada uno de los campos de ésta linea del fichero /etc/password.lorenzo:x:1000:1000:Lorenzo_:/home/lorenzo:/bin/bash Sube los ejerciciosa gitlab. Gitlab sella cuándo se han subido las cosas no lo dejes para otro día.

lorenzo: Es el alias del usuario que utiliza para registrarse.
x: Representa que la contraseña cifrada se encuentra en /etc/shadow
1000: Es el número de identificación del ususario UID.
1000: Representa el número identificador del grupo principal al que pertenece el usuario, lo que se conoce como GID.
Lorenzo,,,: Es la información adicional que has proporcionado al crear la cuenta en cuestión. Así aperecerán el nombre de usuario, y otros datos que defines durante la creación del nuevo usuario.
/home/lorenzo: Es la ruta de inicio del nuevo usuario, el hogar del usuario.
/bin/bash: Es el shell que utiliza el usuario en cuestión.

2. Muestra los usuarios normales.

cat /etc/passwd | grep -v /bin/false | grep -v /nologin

3. Explica grep-v.

"grep" te permite hacer busquedas de palabras o parones en un fichero o grupo de ficheros. Lo que hace grep es buscar las líneas que coinciden y las imprime en la salida estándar.
"-v" invierte el sentido de la búsqueda.

4. Añade el usuario manolito.

sudo adduser <usuario>

5. Bórralo.

sudo deluser <usuario>

6. Explica el comando groups.

Nos dice en que grupos estamos.

7. Explica las opciones de sudo usermod -a -G <grupo> <usario>

 - La opción -a tiene que ser utilizada siempre junto con la opción -G, y lo que indica  es que    añades al usuario a grupos suplementarios.
 - La opción -G te permite indicar a los grupos suplementarios (separados por comas) a los que quieres que pertenezca el usuario.
 - También puedes utilizar la forma usermod -aG <grupos> <usuarios>.

8. REaliza un ejemplo del comando anterior.

sudo usermode -aG cdrom,dialout lorenzo

9. Muestra en tú equipo que ha funcionado.



10. Borra a un usuario de un grupo mediante deluser.

sudo deluser <usuario> <grupo>

11. Explica el  comando finger.

Te ofrece información de los usuarios de tu equipo, en principio, de los usuarios que no son de sistema.



