# os subject
1. ¿Qué es un repositorio?
	Donde se almacena los datos que has instalado.
2. ¿Qué es una rama?
	
3. ¿Son origin y local dos repositorios?
	No.
4. ¿Qué es .gitignore?
	Es para ignorar archivos o directorio.
5. Busca 5 ejemplos de .gitignore en repositorios de gitlab.
	
6. Describe los siguientes comandos.

	-status -> Muestra la la lista de archivos.
	-add -> Se usa para agregar archivos.
	-commit -> Para cambiara a la cabecera.
	-clone -> Se usa para revisar repositorios.
	-push -> Envía los cambios a la rama principal.
	-pull -> Para fusionar todos los cambios.
	-checkout -> Para crear ramas o cambiar entre ellas.
	-branch -> Para crear o borrar ramas.
	-log -> Muestra una lista de commits.
	-merge -> Para convinar una rama con otra.


7. Explica git add -u
	Mira los archivos y si han sido eliminados, no hace ningún cambio.


