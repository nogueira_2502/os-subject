# Preguntas de autoevaluación.

 1. ¿Qué hace el comando? Mira. Explica date man date +%s%3N

   Lo que hace este comando es: 
	 - Date: darte el día.
	 - %s: te dice desde el año 1970-01-01 00:00:00 UTC que 	debe cambiarse.
	 - %3N: te lo muestra en nanosegundos, en este caso serían 		treses.

 2. ¿Qué hace? tara.sh

  Lo que hace es esperar un tiempo y después imprime un 	texto con  la duración de la tarea.

 3. Explica usando los tiempos de el tiempo que te da en el caso de .tarea time secuencial

  Te enseña los segundos de cada tarea.

 4. Explica usando los tiempos de el tiempo que te da en el caso de .tarea time paralelo

  Lanzas las taréas en segundo plano y ejecutas de forma simultánea, han ido terminando según su tiempo de proceso.

## Mira el manual para buscar ayuda en internat. xargs


 1. Explica la sigente línea.

	time echo 4 Tarea1 3 Tarea2 2 Tarea3 1 Tarea4 | xargs -n 2 -P 4 ./tarea.sh

  Lo que el comando hace es:
	- Tarea4 en 1: Indica la ejecución de las tareas.
	- Tarea3 en 2: Indica la ejecución de las tareas.
	- Tarea2 en 1: Indica la ejecución de las tareas.
	- Tarea1 en 4: Indica la ejecución de las tareas.
	- -n 2: Indica los argumentos como debe pasarlos a cada tarea. Así con -n 2, le estoy diciendo que los tiene que agrupar de dos en dos.
	- -P 4: Es para establecer el número de procesos que va a lanzar de forma simultánea.

### Mira la ayuda del comando .parallel

 1. Muestra un ejemplo (pantallazo) como funciona. Incrusta en el archivo de soluciones usando los enlaces de marckdown.

   /home/sergio/work/os-subject/03_19/Captura de pantalla de 2020-04-08 22-45-57.png

 2. Explica la opción -j

  -j: Indica el número máximo de procesos en paralelo.

 3. Explica y muestra un ejemplo de. --link

  --link: Combina un argumento del primer grupo con un argumento del segundo. Si no lo haces de este modo lo que haces es combinar todos, y esto no es lo que queremos.

#### Notas:


 1. Ejecuta algo entre decir que se ejecute un comando y pon aquí su salida.$()

  

 2. Cuando ejecutamos:, dentro del guión todas las apariciones de seán sustituir por por por. En conclusión, y son los parámetros de la línea de comandos ./tarea.sh 1 tarea1 $1 tarea1 $1 $2

  Se sustituiran por tiempos. 

 3. En un script todo lo que empieza por es un cometario.#

  Si, es obligatorio poner la almohadilla (#), la ecepción es (#!).

 4. Cuando la primera línea de un script tiene un shebang indica con qué programas hay que interpretar el contenido del contenido del fichero.#!

  Con un "cat".


